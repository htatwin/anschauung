/*run create_db.sql first
psql -d crypto -U kau -f create_tables.sql*/

DROP TABLE IF EXISTS datasource;
DROP TABLE IF EXISTS cryptoprice;


CREATE TABLE IF NOT EXISTS datasource (
	id SERIAL PRIMARY KEY,
	url TEXT,
	type TEXT,
	name TEXT UNIQUE,
	source TEXT,
	pair TEXT,
	data_field TEXT,
	time_field TEXT,
	deltatime BIGINT,
	status TEXT DEFAULT 'stopped',
    created_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS cryptoprice (
	id SERIAL PRIMARY KEY,
	name TEXT,
	price FLOAT,
	time TIMESTAMP
);
