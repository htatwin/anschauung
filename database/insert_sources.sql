/* 
psql -d crypto -U kau -f insert_sources.sql
*/
INSERT INTO datasource (url, type, name, source, pair, data_field, time_field, deltatime, status ) VALUES ('https://api.coindesk.com/v1/bpi/currentprice/CHF.json', 'rest', 'Coindesk:BTCCHF/1min', 'Coindesk', 'BTCCHF', 'bpi.CHF.rate_float', 'time.updatedISO', 60000);

INSERT INTO datasource (url, type, name, source, pair, data_field, time_field, deltatime ) VALUES ('https://api.coindesk.com/v1/bpi/currentprice.json', 'rest', 'Coindesk:BTCUSD/1min', 'Coindesk', 'BTCUSD', 'bpi.USD.rate_float', 'time.updatedISO', 60000 );