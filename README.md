You need to create a .env in the data-scraping-service folder and add:
```
DB_HOST =   
DB_PORT =
DB_USERNAME =
DB_PASSWORD =
DB_DATABASE =
```
and fill it up.

Refer to database folder for migrations.
