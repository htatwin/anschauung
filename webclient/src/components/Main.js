import React, { Component } from "react";
import "./Main.css";

class Menu extends Component {
	render() {
		return (
			<div className="Menu" onClick={this.props.handleMenu}>
				<button id="AddSourceButton" className="btn-primary">Add source</button>

				<button id="SourcesInfoButton" className="btn-primary">Sources Info</button>
			</div>
		);
	}
}

class SourcesInfo extends Component {
	displaySources() {
		console.log(this.props.sources);

		if (this.props.sources) {
			const elements = this.props.sources.map(val => {
				console.log(val);

				return (
					<div key={val.name} className="sourceDisplay">
						<div>{val.name}</div>
						<div className={val.status}>{val.status}</div>
						<button
                        className="btn-primary"
							data-id={val.name}
							data-buttontype="start"
							onClick={this.props.handleToggleScraping}
						>
							Start
						</button>
						<button
                        className="btn-warning"
							data-id={val.name}
							data-buttontype="stop"
							onClick={this.props.handleToggleScraping}
						>
							Stop
						</button>
					</div>
				);
			});

			return elements;
		} else {
			return <div>You got no sources yet.</div>;
		}
	}

	render() {
		return <div className="SourcesInfo">{this.displaySources()}</div>;
	}
}

class AddSourceForm extends Component {
	render() {
		return (
			<div className="AddSourceForm">
				<form onChange={this.props.handleChange} onSubmit={this.props.onSubmit}>
					<div className="input-container">
						<div>source url:</div>
						<input type="text" name="url" />
					</div>

					<div className="input-container">
						<div />
						type:
						<select name="type">
							<option value=""> --- </option>
							<option value="REST"> REST</option>
							<option value="Websocket" disabled>
								{" "}
								Websocket (not yet implemented...)
							</option>
						</select>
					</div>
					<div className="input-container">
						<div>name:</div>
						<input type="text" name="name" />
						<div>format : Source:Pair/time, i.e: Coinbase:BTCUSD/1min</div>
					</div>
					<div className="input-container">
						<div>source:</div>
						<input type="text" name="source" />
					</div>
					<div className="input-container">
						<div>pair:</div>
						<input type="text" name="pair" />
					</div>
					<div className="input-container">
						<div>data field:</div>
						<input
							type="text"
							name="data_field"
							disabled
							value={this.props.data_field}
						/>
						<div>Make sure it is float or integer.</div>
					</div>
					<div className="input-container">
						<div>time field:</div>
						<input
							type="text"
							name="time_field"
							disabled
							value={this.props.time_field}
						/>
						<div>Only ISO or UTC format.</div>
					</div>
					<div className="input-container">
						<div>deltatime:</div>
						<input type="number" name="deltatime" />
						<div> in milliseconds please. 1000ms === 1 s.</div>
					</div>
					<button type="submit">Submit</button>
				</form>
			</div>
		);
	}
}

class ApiDataDisplay extends Component {
	displayApiData() {
		const apiData = this.props.apiData;

		if (apiData) {
			return this.objectTraverser(apiData);
		}
	}

	objectTraverser(data, parentName = null) {
		if (typeof data === "object" && data) {
            console.log(data,"debugging")

            const keys = Object.keys(data);
            

			const elements = keys.map(val => {
				const name = parentName ? parentName + "." + val : val;
				return (
					<div key={val} name={name} className="expansion">
						{val} {this.objectTraverser(data[val], name)}
					</div>
				);
			});

			return elements;
		} else {
			return (
				<div
					name={parentName}
					className="expansion-end"
					onClick={this.props.handleApiDataClick}
				>
					{data}
				</div>
			);
		}
	}

	render() {
		return <div className="ApiDataDisplay">{this.displayApiData()}</div>;
	}
}

class Main extends Component {
	constructor() {
		super();
		this.state = { infoText: "", eventChain: null };
	}

	componentDidMount() {
		this.sourcesInfoEl = document.querySelector(" .sources-info-wrapper");
		this.addSourceEl = document.querySelector(".add-source-wrapper");

		this.fetchSources();
	}

	handleToggleScraping = e => {
		const name = e.target.dataset.id;
		const type = e.target.dataset.buttontype;

		const payload = {
			name: name,
			type: type
		};

		console.log("payload", payload);

		fetch("http://localhost:9001/data-scrape/toggleScrape", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			mode: "cors",
			body: JSON.stringify(payload)
		})
			.then(res => res.json())
			.then(res => {
                console.log(res);
                this.fetchSources();
			});
	};

	fetchSources() {
		fetch("http://localhost:9001/data-source/sources", {
			method: "GET",
			mode: "cors"
		})
			.then(res => res.json())
			.then(res => {
				console.log(res);
				console.log("fetched sources");
				console.log(this);
				this.setState({ sources: res });
			})
			.catch(err => console.log(err));
	}

	startEventChain(target) {
		console.log("starting chain");

		if (!this.state.eventChain) {
			this.setState({
				infoText: "Click on the data field",
				eventChain: "data_field"
			});
		} else if (this.state.eventChain === "data_field") {
			this.setState({
				infoText: "Click on the time field",
				eventChain: "time_field",
				data_field: target
			});
		} else if (this.state.eventChain === "time_field") {
			this.setState({ infoText: "", eventChain: "end", time_field: target });
		}
	}

	handleApiDataClick = e => {
		const name = e.target.getAttribute("name");

		if (this.state.eventChain) {
			this.startEventChain(name);
		}
	};

	handleChange = e => {
		const target = e.target;
		this.setState({ [target.name]: target.value });

		if (target.name === "url") {
			this.handleSourceURLInput(target.value);
		}

		console.log("handling change");
	};

	handleSourceURLInput = url => {
		fetch(url, {
			method: "GET",
			mode: "cors"
		})
			.then(res => res.json())
			.then(res => {
				this.setState({ apiData: res });
				this.startEventChain();
			})
			.catch(err => console.log(err));
		console.log("got api result");
	};

	handleSubmit = e => {
		e.preventDefault();

		const payload = {
			url: this.state.url,
			type: this.state.type,
			name: this.state.name,
			source: this.state.source,
			pair: this.state.pair,
			data_field: this.state.data_field,
			time_field: this.state.time_field,
			deltatime: this.state.deltatime
		};

		console.log("payload", payload);

		fetch("http://localhost:9001/data-source/addsource", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			mode: "cors",
			body: JSON.stringify(payload)
		})
			.then(res => res.json())
			.then(res => {
				console.log(res);
				this.fetchSources();
			});
		console.log("handling submit");
	};

	handleMenu = e => {
		const target = e.target.id;
		if (target === "AddSourceButton") {
			console.log(this);
			this.addSourceEl.style.display = "flex";
			this.sourcesInfoEl.style.display = "none";
		} else if (target === "SourcesInfoButton") {
			this.addSourceEl.style.display = "none";
			this.sourcesInfoEl.style.display = "flex";
		}
	};

	render() {
		return (
			<div className="Main">
				<Menu handleMenu={this.handleMenu} />
				<div className="InfoBox">{this.state.infoText}</div>

				<div className="add-source-wrapper">
					<AddSourceForm
						handleChange={this.handleChange}
						onSubmit={this.handleSubmit}
						data_field={this.state.data_field}
						time_field={this.state.time_field}
					/>
					<ApiDataDisplay
						apiData={this.state.apiData}
						handleApiDataClick={this.handleApiDataClick}
					/>
				</div>

				<div className="sources-info-wrapper">
					<SourcesInfo
						sources={this.state.sources}
						handleToggleScraping={this.handleToggleScraping}
					/>
				</div>
			</div>
		);
	}
}

export default Main;
