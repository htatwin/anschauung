const DataSourceController = require("./data_source/data_source_controller");
const DataScrapingController = require("./data_scraping/data_scraping_controller")

module.exports = {DataSourceController, DataScrapingController };