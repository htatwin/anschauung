const { DataSource } = require("../../services/index");

class DataSourceController {
	constructor() {
		this.DataSource = new DataSource();

        this.add = this.add.bind(this);
        this.getAll = this.getAll.bind(this);
	}

	root(req, res) {
		res.status(200).send({
			message:
				"Welcome to the data source root endpoint of the data scrapping service."
		});
	}

	add(req, res) {
		this.DataSource.add(req, res);
    }
    
    getAll(req, res) {
		this.DataSource.getAll(req, res);
	}
}

module.exports = DataSourceController;
