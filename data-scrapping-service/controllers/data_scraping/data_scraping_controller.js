const { CryptoPrice } = require("../../services/index");


class DataScrapingController {
	constructor() {
        this.CryptoPrice = new CryptoPrice();

        this.toggleScrape = this.toggleScrape.bind(this);

        this.startScraping = this.startScraping.bind(this);
        this.stopScraping = this.stopScraping.bind(this);
	}

	root(req, res) {
		res.status(200).send({
			message:
				"Welcome to the data scraping root endpoint of the data scrapping service."
		});
	}

    toggleScrape(req,res){
        this.CryptoPrice.toggleScrape(req,res)
    }

	startScraping(req,res){

		this.CryptoPrice.startScraping(req,res)

    }

    stopScraping(req,res){
        this.CryptoPrice.stopScraping(req,res)
    }
}

module.exports = DataScrapingController;
