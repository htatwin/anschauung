const { db, pgp } = require("../../helpers/db");

const https = require("https");
const http = require("http");

class ScrapeObject {
	constructor(data) {
		this.name = data.name;
		this.url = data.url;
		this.dataField = this.readField(data.data_field);
		this.timeField = this.readField(data.time_field);
		this.deltaTime = parseInt(data.deltatime); //in milliseconds

		this.https = this.url.includes("https");

		this.lastScraped = null;
	}

	async scrape() {
		const now = Date.now();

		if (!this.lastScraped) {
			this.lastScraped = now;
			return await this.makeApiCall();
		} else if (now - this.lastScraped > this.deltaTime) {
			this.lastScraped = now;
			return this.makeApiCall();
		} else {
			return null;
		}
	}

	makeApiCall() {
		if (this.https) {
			const promise = new Promise((resolve, reject) => {
				https
					.get(this.url, res => {
						let data = "";

						// store chunk
						res.on("data", chunk => {
							data += chunk;
						});

						res.on("end", () => {
							data = JSON.parse(data);

							let result = {
								name: this.name,
								time: this.parseData(this.timeField, data),
								price: this.parseData(this.dataField, data)
							};

							resolve(result);
						});
					})
					.on("error", err => {
						console.log("Error: " + err.message);
					});
			});
			return promise;
		} else {
			const promise = new Promise((resolve, reject) => {
				http
					.get(this.url, res => {
						let data = "";

						// store chunk
						res.on("data", chunk => {
							data += chunk;
						});

						res.on("end", () => {
							data = JSON.parse(data);

							let result = {
								name: this.name,
								time: this.parseData(this.timeField, data),
								price: this.parseData(this.dataField, data)
                            };
                            
                            if (typeof result.time === "number"){
                                result.time =new Date(result.time*1000).toISOString();
                            }

							resolve(result);
						});
					})
					.on("error", err => {
						console.log("Error: " + err.message);
					});
			});
			return promise;
		}
	}

	// string parsing and destructuring result data

	readField(str) {
		let arr = str.split(".");
		let arrLength = arr.length;

		//add length of arr to first value of arr so that it is easily referenced
		arr.unshift(arrLength);
		return arr;
	}

	parseData(field, data) {
		let tempCopy = data;
		for (let i = 1; i <= field[0]; i++) {
			tempCopy = tempCopy[field[i]];
		}
		return tempCopy;
	}
}

class CryptoPrice {
	constructor() {
		this.ScrapeObjectList = new Map();
		this.scrapedResultArr = [];

		this.initialized = false;

		this.populateScrapeObjectList();

		this.startScraping();
	}

	// public (for controllers)

	toggleScrape(req, res) {
		const name = req.body.name;
		const type = req.body.type;

		console.log("from req", name, type);

		db.one("select * from datasource where name = $1", name).then(val => {
			if (val.status === "running" && type === "stop") {
				console.log("stop the scrapeObj");
				db.one(
					"update datasource set status = $1 where name = $2 returning *",
					["stopped", name]
				).then(val => {
					//remove the scrapeObj now
					this.ScrapeObjectList.delete(name);
					return res.send({ message: "Stopped the scraping" });
				});
			} else if (val.status === "stopped" && type === "start") {
				console.log("start the scrapeObj");
				db.one(
					"update datasource set status = $1 where name = $2 returning *",
					["running", name]
				).then(val => {
					//add the scrapeObj now
					this.addScrapeObject(val);
					return res.send({ message: "Started the scraping" });
				});
			} else {
				return res.send({ message: "It is already doing what u wanna do" });
			}
		});
	}

	startScraping(req, res) {
		if (!this.initialized) {
			this.initialized = this.scrapingLoop();

			res ? res.send("started scraping") : console.log("started scraping");
		} else {
			//           res.send("already scraping");
			res ? res.send("already scraping") : console.log("already scraping");
		}
	}

	stopScraping(req, res) {
		if (!this.initialized) {
			res.send("scraping have not even started");
		} else {
			clearInterval(this.initialized);
			this.initialized = false;
			res.send("stopped scraping");
		}
	}

	// private

	depopulateScrapeObjectList() {
		this.ScrapeObjectList.clear();
	}

	populateScrapeObjectList() {
		// query database for all sources

		db.any("select * from datasource").then(val => {
			const results = val;

			// loops results and run addScrapeObject to create and add to list
			for (let idx in results) {
				const item = results[idx];

				//checks if status is set to running first
				if (item.status === "running") {
					this.addScrapeObject(item);
				}
			}
		});
	}

	addScrapeObject(item) {
		const scrapeObj = new ScrapeObject(item);
		this.ScrapeObjectList.set(scrapeObj.name, scrapeObj);
	}

	scrapingLoop() {
		let counter = 0;
		return setInterval(async () => {
			//loop through objs to be scraped and scrape them
			for (let i of this.ScrapeObjectList.values()) {
				let result = await i.scrape();

				//no result if not time yet for that particular obj
				if (result) {
					this.scrapedResultArr.push(result);
					//            console.log(this.scrapedResultArr)
				}
			}

			// send results to the database
			// TODO pop to another arr, send that arr
			// ideally publish it to kafka, TODO
			//            console.log(this.scrapedResultArr)
			if (this.scrapedResultArr.length !== 0) {
				const query = pgp.helpers.insert(
					this.scrapedResultArr,
					["name", "price", "time"],
					"cryptoprice"
				);
				// empties the arr
				console.log(this.scrapedResultArr);
				this.scrapedResultArr = [];
				db.none(query).catch(err => console.log(err));
			}

			counter++;
			if (counter % 10 == 0) {
				//				console.log("scraping, counter= ", counter);
			}
		}, 1000);
	}
}

module.exports = CryptoPrice;
