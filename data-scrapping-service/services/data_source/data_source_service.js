const { db, pgp } = require("../../helpers/db");

class DataSource {
	constructor() {}

	add(req, res) {
		console.log(req.body);
		// db.any("select * from datasource").then (queryResult => {
		//     console.log(queryResult)
		// });
		const query = pgp.helpers.insert(
			req.body,
			Object.keys(req.body),
			"datasource"
		);
		db.none(query).then(()=>{

            res.send({ message: "ok" });

        })
	}

	getAll(req, res) {
		console.log("sending all");
		db.any("select * from datasource order by id asc").then(val => {
			res.send(val);
		});
	}
}

module.exports = DataSource;
