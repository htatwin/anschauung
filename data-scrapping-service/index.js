require('dotenv').config();

const express = require("express");
const bodyParser= require('body-parser');
const cors= require('cors')
const Routes = require('./routes/routes')

const hostname = '127.0.0.1';
const port = 9001;

const app = express();

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const router = new Routes(app);
router.init();

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});