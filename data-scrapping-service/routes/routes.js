const { DataSourceController , DataScrapingController } = require("../controllers/index");


class Routes {
	constructor(app) {
        this.DataSourceController = new DataSourceController();
        this.DataScrapingController = new DataScrapingController();
		this.app = app;
    }
    
    init(){

        // DATA SOURCES ROUTES
        this.app.get("/data-source/", this.DataSourceController.root);

        this.app.get("/data-source/sources", this.DataSourceController.getAll)

        this.app.post("/data-source/addsource", this.DataSourceController.add);


        // DATA SCRAPING ROUTES
        this.app.get("/data-scrape",this.DataScrapingController.root);


        this.app.post("/data-scrape/toggleScrape",this.DataScrapingController.toggleScrape)



        // the stop and start ENTIRE process
        this.app.get("/data-scrape/startScraping",this.DataScrapingController.startScraping);
        this.app.get("/data-scrape/stopScraping",this.DataScrapingController.stopScraping);

    }
}

module.exports = Routes;
